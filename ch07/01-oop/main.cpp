#include <iostream>
#include <fstream>
#include <string>
#include <utility>

using namespace std;

class Student {
private:
    int ID{};
    string name;
    double grade{};
    string phone;

    static int maxID;

public:
    Student() = default;

    Student(string name, double grade, string phone) {
        this -> ID = maxID++;
        this -> name = move(name);
        this -> grade = grade;
        this -> phone = move(phone);
    }

    static void printHello() {
        cout << "Hello" << endl;
    }

    int getID() {
        return ID;
    }

    [[nodiscard]] const string &getName() const {
        return name;
    }

    void setName(const string &aName) {
        name = aName;
    }

    [[nodiscard]] double getGrade() const {
        return grade;
    }

    void setGrade(double aGrade) {
        grade = aGrade;
    }

    [[nodiscard]] const string &getPhone() const {
        return phone;
    }

    void setPhone(const string &aPhone) {
        phone = aPhone;
    }

    string toString() {
        string result;
        result += "ID: " + to_string(ID) + "\n";
        result += "Name: " + name + "\n";
        result += "Grade: " + to_string(grade) + "\n";
        result += "Phone: " + phone + "\n";
        return result;
    }
};
int Student::maxID = 1;

class StudentRecord {
private:
    Student students[10];
    int numberOfStudents = 0;

public:
    int getNumberOfStudents() {
        return numberOfStudents;
    }

    void readData(const string& filename) {
        ifstream f;
        f.open(filename);
        numberOfStudents = 0;
        string line;
        int state = 0;
        string name;
        double grade = 0.0;
        string phone;

        while (getline(f, line)) {
            state %= 4;
            switch (state) {
                case 0:
                    break;
                case 1:
                    name = line;
                    break;
                case 2:
                    grade = stod(line);
                    break;
                case 3:
                    phone = line;
                    break;
                default:
                    cout << "unexpected state: " << state << endl;
                    break;
            }
            if (state == 3) {
                Student st(name, grade, phone);
                this -> students[numberOfStudents] = st;
                numberOfStudents += 1;
            }
            state++;
        }
        f.close();
    }

    void addStudent(Student st) {
        students[numberOfStudents] = move(st);
        numberOfStudents++;
    }

    void addStudent(int id, const string& name, float grade, const string& phone) {
        addStudent(Student(name, grade, phone));
    }

    void removeStudent(int id) {
        for (int i = 0; i < numberOfStudents; i++) {
            if (students[i].getID() == id) {
                for (int j = i; j < numberOfStudents - 1; j++) {
                    students[j] = students[j + 1];
                }
                break;
            }
        }
        numberOfStudents--;
    }

    void printData() {
        for (int i = 0; i < numberOfStudents; i++) {
            cout << students[i].toString() << endl;
        }
    }
};


int main() {
    StudentRecord record;
    record.readData("../input.txt");

    Student st("", 0.0, "");
    st.setName("Ben");
    st.setGrade(9.7);
    st.setPhone("555 987");
    record.addStudent(st);

    record.addStudent(6, "Yoda", 10.0, "555 876");
    record.printData();
    cout << "=========================" << endl;

    record.removeStudent(5);
    record.printData();
    cout << "=========================" << endl;

    record.removeStudent(6);
    record.printData();
    cout << "=========================" << endl;

    cout << "Number of students: " << record.getNumberOfStudents() << endl;
    cout << st.getName() << ", " << st.getGrade() << ", " << st.getPhone() << endl;
    Student::printHello();

    return 0;
}
