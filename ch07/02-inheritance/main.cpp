#include <iostream>
#include <utility>

using namespace std;

class Animal {
private:
    string name;
    int age{};

public:
    Animal() = default;
    Animal(string name, int age) {
        this->name = move(name);
        this->age = age;
    }

    string toString() {
        return "Name: " + name + "\n" + "Age: " + to_string(age) + "\n";
    }

    virtual void makeSound() {
        cout << "sound" << endl;
    }
};

class Dog : public Animal {
public:
    Dog(string name, int age) : Animal(move(name), age) {}

    void makeSound() override {
        cout << "Bark" << endl;
    }
};

class Cat : public Animal {
public:
    Cat(string name, int age) : Animal(move(name), age) {}

    void makeSound() override {
        cout << "Miauu" << endl;
    }
};

class Horse : public Animal {
public:
    Horse(string name, int age) : Animal(move(name), age) {}

    void makeSound() override {
        cout << "Pfff" << endl;
    }
};

class Fox : public Animal {
public:
    Fox(string name, int age) : Animal(move(name), age) {}

    void makeSound() override {
        cout << "Whouf" << endl;
    }
};

int main() {
    Dog d("Spot", 2);
    cout << d.toString() << endl;

    Cat c("Puffy", 3);
    Horse h("Steve", 6);
    Fox f("Red", 5);

    Animal *v[] = {&d, &c, &h, &f};
    for (Animal *p : v) {
        p->makeSound();
    }

    return 0;
}
