cmake_minimum_required(VERSION 3.15)
project(01_more_variables)

set(CMAKE_CXX_STANDARD 17)

add_executable(01_more_variables main.cpp)