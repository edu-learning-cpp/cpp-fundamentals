#include <iostream>
#include <cstdlib>
#include <fstream>
#include <string>

using namespace std;

void sizes() {
    cout << sizeof(int) << endl;
    cout << sizeof(uint) << endl;
    cout << sizeof(float) << endl;
    cout << sizeof(char) << endl;
    cout << sizeof(string) << endl;

    char a = 'a';
    cout << "Size of " << a << " is " << sizeof(a) << endl << endl;
}

void integers() {
    uint y = -10;
    cout << "Value of -10 assigned to uint is " << y << endl;

    int x = 2500000000;
    cout << "Value of 2500000000 assigned to int is " << x << endl;
    y = 2500000000;
    cout << "Value of 2500000000 assigned to uint is " << y << endl;
    y = 4500000000;
    cout << "Value of 4500000000 assigned to uint is " << y << endl << endl;

    cout << sizeof(long) << endl;
    long z = 2500000000;
    cout << "Value of 2500000000 assigned to long is " << z << endl;
    long w = 4500000000;
    cout << "Value of 4500000000 assigned to long is " << w << endl << endl;

    int64_t b = 2500000000;
    cout << "Value of 2500000000 assigned to int64_t is " << b << endl;
    uint64_t c = 4500000000;
    cout << "Value of 4500000000 assigned to uint64_t is " << c << endl;
    cout << "size of int64_t is " << sizeof(int64_t) << endl << endl;
}

void floatingNumbers() {
    double x = 10.00;
    cout << x << endl;
    cout << "Size of double is " << sizeof(double) << endl;
    cout << "Size of float is " << sizeof(float) << endl << endl;
}

string getFormattedInfo(int id, string name[], double grade[], string phone[]) {
    string result;
    result += "Name: " + name[id] + "\n";
    result += "Grade: " + to_string(grade[id]) + "\n";
    result += "Phone: " + phone[id] + "\n";
    return result;
}

struct Student {
    int ID{};
    string name;
    double grade{};
    string phone;
};

string getInfo(const Student& student) {
    string result;
    result += "Name: " + student.name + "\n";
    result += "Grade: " + to_string(student.grade) + "\n";
    result += "Phone: " + student.phone + "\n";
    return result;
}

void readData(Student students[], int &numberOfStudents, string filename) {
    ifstream f;
    f.open(filename);
    numberOfStudents = 0;
    string line;
    int state = 0;

    while (getline(f, line)) {
        state %= 4;
        switch (state) {
            case 0:
                students[numberOfStudents].ID = stoi(line);
                break;
            case 1:
                students[numberOfStudents].name = line;
                break;
            case 2:
                students[numberOfStudents].grade = stod(line);
                break;
            case 3:
                students[numberOfStudents].phone = line;
                break;
        }
        if (state == 3) {
            numberOfStudents += 1;
        }
        state++;
    }
    f.close();
}

void students() {
    int studentID[] = {0, 1, 2, 3};
    string names[] = {"Luke", "Leia", "Han", "Lando"};
    double grades[] = {8.2, 8.1, 9.0, 8.7};
    string phoneNumber[] = {"555 555", "555 675", "555 872", "555 432"};

    int id = studentID[0];

    cout << getFormattedInfo(id, names, grades, phoneNumber) << endl;

    Student s1;
    s1.ID = 4;
    s1.name = "Padme";
    s1.grade = 9.9;
    s1.phone = "555 123";
    cout << getInfo(s1) << endl;

    Student students[10];
    int numberOfStudents = 0;
    readData(students, numberOfStudents, "../input.txt");
    for (int i = 0; i < numberOfStudents; i++) {
        cout << getInfo(students[i]) << endl;
    }
}

union myUnion {
    char c;
    int i;
    long l;
    float f;
};

void unionTypes() {
    myUnion u;
    u.i = 5;
    u.f = 5.6;
    cout << sizeof(myUnion) << endl;
}

void matrices() {
    int matrix[][3] = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
    int rows = 3;
    int cols = 3;

    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            cout << matrix[i][j] << " ";
        }
        cout << endl;
    }
    cout << endl;
}

int lastDigit(int &x) { // passing parameter by reference
    int l = x % 10;
    x /= 10;
    return l;
}

int main() {
    sizes();
    integers();
    floatingNumbers();
    students();
    unionTypes();
    matrices();

    int x = 12345;
    cout << lastDigit(x) << endl;
    cout << x << endl;

    return 0;
}
