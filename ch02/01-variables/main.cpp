#include <iostream>

using namespace std;

int main() {
    // numbers
    int x = 5;
    cout << "x = " << x << endl;
    x = 20.7;
    cout << "x = " << x << endl;

    float y = 5.6;
    cout << "y = " << y << endl;
    y = 5;
    cout << "y = " << y << endl;

    x = 10;
    y = 5.6;
    cout << "x + y = " << x + y << endl << endl;

    // char
    char a = 'A';
    char b = 'B';

    cout << a << " " << b << endl;

    char c = 67;
    cout << "c = " << c << endl << endl;

    // string
    string str1 = "Hello";
    string str2 = "World";
    cout << str1 + " " + str2 << endl << endl;

    // operations
    int z = 5, w = 6;
    cout << z << " + " << w << " = " << z + w << endl;
    cout << z << " - " << w << " = " << z - w << endl;
    cout << z << " * " << w << " = " << z * w << endl;
    cout << z << " / " << w << " = " << z / w << endl;

    cout << z * 2 + w * 3 << endl << endl;

    // logical operators
    bool var1 = true;
    bool var2 = false;
    cout << var1 << " " << var2 << endl;

    var1 = 10;
    var2 = 0;
    cout << var1 << " " << var2 << endl;
    var1 = -100;
    cout << var1 << " " << var2 << endl;

    var1 = true;
    var2 = false;
    cout << var1 << " AND " << var2 << " = " << (var1 && var2) << endl;
    cout << var1 << " AND " << true << " = " << (var1 && true) << endl;
    cout << var1 << " OR " << var2 << " = " << (var1 || var2) << endl;
    cout << false << " OR " << var2 << " = " << (false || var2) << endl << endl;

    // modulo operator
    x = 4;
    z = 2;
    bool isXEven = (x % 2 == 0);
    cout << x << " % " << z << " = " << x % z << endl;
    cout << "is x even? " << isXEven << endl;

    x = 5;
    isXEven = (x % 2 == 0);
    cout << x << " % " << z << " = " << x % z << endl;
    cout << "is x even? " << isXEven << endl << endl;

    // comparison operators
    cout << x << " == " << z << " is " << (x == z) << endl;
    cout << x << " != " << z << " is " << (x != z) << endl;
    cout << x << " > " << z << " is " << (x > z) << endl;
    cout << x << " >= " << z << " is " << (x >= z) << endl;
    cout << x << " < " << z << " is " << (x < z) << endl;
    cout << x << " <= " << z << " is " << (x <= z) << endl;
    z = 5;
    cout << x << " == " << z << " is " << (x == z) << endl;
    cout << x << " != " << z << " is " << (x != z) << endl;
    cout << x << " > " << z << " is " << (x > z) << endl;
    cout << x << " >= " << z << " is " << (x >= z) << endl;
    cout << x << " < " << z << " is " << (x < z) << endl;
    cout << x << " <= " << z << " is " << (x <= z) << endl << endl;

    /*
     * De Morgan's Law
     * !(a AND b) == !a OR !b
     * !(a OR b) == !a AND !b
     */
    var1 = true;
    var2 = false;
    cout << "!( " << var1 << " AND " << var2 << " ) == " << !(var1 && var2) << " # ";
    cout << "!" << var1 << " OR " << "!" << var2 << " == " << (!var1 || !var2) << endl;
    cout << "!( " << var1 << " OR " << var2 << " ) == " << !(var1 || var2) << " # ";
    cout << "!" << var1 << " AND " << "!" << var2 << " == " << (!var1 && !var2) << endl << endl;

    // arrays
    float grades[100];
    grades[0] = 8.7;
    grades[1] = 8.9;
    cout << grades[0] << " " << grades[1] << endl;
    cout << grades[10] << endl;
    cout << grades[105] << endl;

    grades[105] = 42;
    cout << grades[105] << endl << endl;

    // pointers
    x = 5;
    cout << "value of x = " << x << endl;
    cout << "address of x = " << &x << endl;
    cout << "The value at address: " << &x << " is " << x << endl;

    int *p = &x;
    cout << "The value at address: " << p << " is " << *p << endl;
    cout << "The value at address: " << &p << " is " << p << endl;

    int **q = &p;
    cout << "The value at address: " << q << " is " << *q << endl;
    cout << "The value at address: " << *q << " is " << **q << endl;

    float grades1[100];
    grades1[0] = 9.0;
    grades1[1] = 7.4;
    cout << grades1[0] << ", " << grades1[1] << endl;

    float *grades2 = grades1;
    cout << grades2[0] << ", " << grades2[1] << endl;
    cout << 0[grades2] << ", " << 1[grades2] << endl;

    return 0;
}
