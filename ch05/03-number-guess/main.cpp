#include <iostream>

using namespace std;

void guessMyNumber(int uLimit) {
    int lLimit = 1;
    while (lLimit < uLimit) {
        int guess = (uLimit + lLimit) / 2;
        cout << "Your number is: " << guess << endl;
        string option;
        cout << "Select the option: correct, smaller or bigger" << endl;
        cin >> option;
        if (option == "correct") {
            cout << "Thank you for playing" << endl;
            break;
        } else if (option == "smaller") {
            uLimit = guess;
        } else if (option == "bigger") {
            lLimit = guess;
        } else {
            cout << "I don't understand that" << endl;
        }
    }
}

int main() {
    guessMyNumber(100);
    return 0;
}
