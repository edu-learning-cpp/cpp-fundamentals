#include <iostream>

using namespace std;

void fizzball(int x) {
    for (int i = 1; i <= x; i++) {
        if (i % 5 == 0 && i % 7 == 0) {
            cout << "fizzball ";
        } else if (i % 7 == 0) {
            cout << "ball ";
        } else if (i % 5 == 0) {
            cout << "fizz ";
        } else {
            cout << i << " ";
        }
    }
    cout << endl;
}

int main() {
    fizzball(50);
    return 0;
}
