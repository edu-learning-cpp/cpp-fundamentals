#include <iostream>
#include <fstream>

using namespace std;

void readInputFile(const string& filename) {
    ifstream f;
    f.open(filename);
    int n;
    int v[10];
    f >> n;
    for (int i = 0; i < n; i++) {
        f >> v[i];
    }
    for (int i = 0; i < n; i++) {
        cout << v[i] << " ";
    }
    cout << endl;

    f.close();
}

void readInputFileWhile(const string& filename) {
    ifstream f;
    f.open(filename);
    int n = 0;
    int v[10];
    while (!f.eof()) {
        f >> v[n];
        n++;
    }
    for (int i = 0; i < n; i++) {
        cout << v[i] << " ";
    }
    cout << endl;

    f.close();
}

void readAndOutput(const string& filename) {
    ifstream f;
    f.open(filename);
    int n = 0;
    int v[10];
    while (f >> v[n]) {
        n++;
    }
    f.close();

    ofstream g("../output.txt");
    for (int i = 0; i < n; i++) {
        g << v[i] << " ";
    }
    g.close();
}

int main() {
    readInputFile("../input.txt");

    readInputFileWhile("../input2.txt");

    readAndOutput("../input2.txt");

    return 0;
}
