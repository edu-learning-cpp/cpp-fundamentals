#include <iostream>
#include <string>
#include <chrono>
#include <thread>
#include <cstdlib>
#include <ctime>

using namespace std;
using namespace this_thread;
using namespace chrono;

void magic8BallGame() {
    string answers[] = {"Yes", "No", "Maybe", "It is not clear", "I don't know", "42"};
    while (true) {
        string option;
        cout << "Do you want to ask question? (yes/no)" << endl;
        cin >> option;
        if (option == "yes") {
            cout << "You may ask your question" << endl;
            string question;
            cin.ignore();
            getline(cin, question);
            cout << "hmmm..." << endl;
            sleep_for(milliseconds(1000));
            string answer;
            while (answer.empty()) {
                for (const string& ans : answers) {
                    if (rand() % 10 == 1) {
                        answer = ans;
                    }
                }
            }
            cout << answer << endl;
        } else if (option == "no") {
            cout << "Goodbye" << endl;
            break;
        } else {
            cout << "I don't understand" << endl;
        }
    }
}

int main() {
    srand(time(nullptr));
    magic8BallGame();
    return 0;
}
