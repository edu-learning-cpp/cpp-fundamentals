#include <iostream>
#include <string>

using namespace std;

int main() {
    string name;
    int age;
    cout << "Please input your name: ";
    // cin >> name; // this can read only single string, whitespace is treated as new input
    getline(cin, name); // this reads entire line including whitespaces
    cout << endl;
    cout << "Please input your age: ";
    cin >> age;
    cout << endl;
    cout << "Profile created!" << endl;
    cout << "Name: " << name << endl;
    cout << "Age: " << age << endl;
    return 0;
}
