#include <iostream>

using namespace std;

void printStudentData(int studentId, string names[], float grades[], int year[], string phoneNumber[]) {
    cout << "Student name: " << names[studentId] << endl;
    cout << "Student grade: " << grades[studentId] << endl;
    cout << "Student year: " << year[studentId] << endl;
    cout << "Student phone number: " << phoneNumber[studentId] << endl << endl;
}

int add(int x, int y) {
    return x + y;
}

int factorial(int x) {
    int result = 1;
    for (int i = 2; i <= x; i++) {
        result *= i;
    }
    return result;
}

int lastDigitCStyle(int *x) {
    int last = *x % 10;
    *x /= 10;
    return last;
}

int lastDigit(int &x);

int factorialRec(int n);

int main() {
    int studentID[] = {0, 1, 2, 3};
    string names[] = {"Luke", "Leia", "Han", "Lando"};
    float grades[] = {8.2, 8.1, 9.0, 8.7};
    int year[] = {1, 2, 1, 2};
    string phoneNumber[] = {"555 555", "555 675", "555 872", "555 432"};

    int id = studentID[0];
    cout << "Student name: " << names[id] << endl;
    cout << "Student grade: " << grades[id] << endl;
    cout << "Student year: " << year[id] << endl;
    cout << "Student phone number: " << phoneNumber[id] << endl << endl;

    printStudentData(1, names, grades, year, phoneNumber);
    printStudentData(2, names, grades, year, phoneNumber);

    cout << add(2, 3) << endl << endl;

    cout << factorial(5) << endl;
    cout << factorial(1) << endl;
    cout << factorial(0) << endl << endl;

    int x = 1234;
    cout << "Last digit of " << x << " is " << lastDigitCStyle(&x) << endl;
    cout << "x is now equal to " << x << endl;
    cout << "Last digit of " << x << " is " << lastDigit(x) << endl;
    cout << "x is now equal to " << x << endl << endl;

    cout << factorialRec(5) << endl;
    cout << factorialRec(1) << endl;
    cout << factorialRec(0) << endl;

    return 0;
}

int factorialRec(int n) {
    if (n < 1) {
        return 1;
    }
    if (n == 1) {
        return n;
    }
    return n * factorialRec(n - 1);
}

int lastDigit(int &x) {
    int last = x % 10;
    x /= 10;
    return last;
}
