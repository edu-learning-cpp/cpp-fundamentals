#include <iostream>

using namespace std;

int main() {
    // if-else statements
    int x = 20;
    if (x % 2 == 0) {
        cout << "The number is even" << endl;
    } else {
        cout << "The number is not even" << endl;
    }

    x = 21;
    if (x % 2 == 0) {
        cout << "The number is even" << endl;
    } else {
        cout << "The number is not even" << endl;
    }

    if (x >= 0 && x <= 19) {
        cout << x << endl;
    } else if (x <= 29) {
        cout << x / 2 << endl;
    } else {
        cout << x << " is over 30" << endl;
    }

    // switch
    int month = 6;
    switch (month) {
        case 1:
            cout << "January";
            break;
        case 2:
            cout << "February";
            break;
        case 3:
            cout << "March";
            break;
        case 4:
            cout << "April";
            break;
        case 5:
            cout << "May";
            break;
        case 6:
            cout << "June";
            break;
        case 7:
            cout << "July";
            break;
        case 8:
            cout << "August";
            break;
        case 9:
            cout << "September";
            break;
        case 10:
            cout << "October";
            break;
        case 11:
            cout << "November";
            break;
        case 12:
            cout << "December";
            break;
        default:
            cout << "Invalid month";
            break;
    }
    cout << endl;

    // loops
    string names[] = {"Edu", "Janna", "Arno", "Lauri", "Alisa", "Karoliina"};

    for (auto i = 0; i < 6; i++) {
        cout << names[i] << " ";
    }
    cout << endl;

    for (const auto& name : names) {
        cout << name << " ";
    }
    cout << endl;

    x = 123456;
    int y = 0;
    cout << x << endl;
    while (x != 0) {
        int l = x % 10;
        x = x / 10;
        y = y * 10 + l;
    }
    cout << y << endl << endl;

    x = 0;
    cout << "while" << endl;
    while (x != 0) {
        cout << x << endl;
    }
    cout << "do while" << endl;
    do {
        cout << x << endl;
    } while (x != 0);
    cout << "end" << endl;

    return 0;
}
